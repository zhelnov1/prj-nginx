provider "aws" {

  region = "eu-central-1"


}


terraform {
  backend "s3" {
    bucket = "maksimz-project-terraform-state"
    key    = "dev/prj-nginx-prod/terrafor.tfstate"
    region = "eu-central-1"
  }
}



data "aws_ami" "latest_ubuntu" {
  owners      = ["099720109477"]
  most_recent = true
  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-jammy-22.04-amd64-server-*"]
  }
}

resource "aws_instance" "my_webserver" {
  ami                    = data.aws_ami.latest_ubuntu.id # Ubuntu server ami
  instance_type          = "t3.micro"
  vpc_security_group_ids = [aws_security_group.my_webserver.id]


  tags = {
    Name    = "My Web Server"
    Owner   = "Maksim"
    Project = "Made by Terraform"
  }
  
  lifecycle {
    create_before_destroy = true
  }

}

resource "aws_security_group" "my_webserver" {
  name = "Dynamic Security Group"



  dynamic "ingress" {
    for_each = ["80", "443", "22", "3000", "2376"]
    content {
      from_port   = ingress.value
      to_port     = ingress.value
      protocol    = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
    }
  }





  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
    #   ipv6_cidr_blocks = ["::/0"]
  }

  tags = {
    Name    = "Dynamic Security Group"
    Owner   = "Maksim"
    Project = "Made by Terraform"
  }
}
